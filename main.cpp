#include <iostream>
//#include <inc/Structures.h>
#include <queue>
#include <cstdlib>

#define M_limit 50          //ograniczenie górnej granicy losowanych elementów
#define W_limit 100         //Ograniczenie górne wartości losowanych współczynników

using namespace std;




int LOS(int limit){ int temp; temp=rand()%limit; return temp;}

int main(){
    srand( time( NULL ) );
    queue<int> kolejka;
    int temp,N,K,J;

    cout<<"how many numbers shall i randomize?"<<endl;
    cin>>N;
    while (N>M_limit || N<0) {
        cout<<"You chose out of range (minimum=0 maximum="<<M_limit<<")\ntry with number within shown range"<<endl;
        cin>>N;
    }

    cout<<"randomizing your numbers.."<<endl;
    for(size_t i = 0; i < N; i++)
    {
        kolejka.push(LOS(W_limit));
    }
    
    cout<<"done \nshowing first number of your queue\n"<<kolejka.front()<<endl;
    cout<<"how many numbers you want to discard?"<<endl;
    cin>>K;
    while (K>(kolejka.size())|| K<0) {
        cout<<"You chose out of range (minimum=0 maximum="<<(kolejka.size())<<")\ntry with number within shown range"<<endl;
        cin>>K;
    }
    
    cout<<"discarding those numbers"<<endl;
    for(size_t i = 0; i < K; i++)
    {
        kolejka.pop();
    }
    cout<<"done"<<endl;
    cout<<"how many numbers you want to push to your queue?"<<endl;
    cin>>J;
    while (J>(10)|| J<0) {
        cout<<"You chose out of range (minimum=0 maximum=10)\ntry with number within shown range"<<endl;
        cin>>J;
    }

    for(size_t i = 0; i < J; i++)
    {

        //cout<<i<<" "<<J<<" "<<kolejka.size()<<endl;
        if (i==0)   cout<<"enter your first number"<<endl;
        else        cout<<"enter your next number"<<endl;
        cin>>temp;
        kolejka.push(temp);
    }
    
cout<<"First element in your queue is:"<<kolejka.front()<<endl;

cout<<"queue size:"<<kolejka.size()<<endl;



 return 0;   
}